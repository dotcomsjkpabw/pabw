<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/img/gaya.css">

    <title>Lihat Program</title>
  </head>
  <body>
    
    <!-- navbar -->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-light">
          <a class="navbar-brand">SIMANPROS</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="lipro_kor" class="nav-link active">Program</a>
              </li>
              <li class="nav-item">
                <a href="proposal" class="nav-link">Pengajuan</a>
              </li>
              <li class="nav-item">
                <a href="login" class="nav-link">Logout</a>
              </li>
            </ul>
          </div>
        </nav>
    <!-- akhir navbar -->

    <!-- lihat program -->
    <section class="lipro" id="lipro">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 offset-sm-1">
            <h1>Lihat Program</h1>
            <p>Mungkin anda perlu untuk melihat beberapa program-program siaga bencana yang telah diajukan oleh korporasi sebelumnya</p>
          </div>
        </div>
      </div>
    </section>
    <!-- akhir lihat program -->

    <!-- cari program -->
    <nav class="navbar navbar-light bg-light justify-content-center">
      <form class="form-inline">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Cari Program" aria-label="Cari Program" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">Cari</span>
          </div>
        </div>
      </form>
    </nav>
    <!-- akhir cari program -->

    <!-- isi -->
    <section class="isi" id="isi">
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <h5>Fitur Pencarian : </h5>
            <div class="dropdown" width="100%">
              <button class="btn dropdown-toggle" type="button" id="pilih" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Pilih Perusahaan</button>
              <div class="dropdown-menu" aria-labelledby="pilih">
                <a href="#" class="dropdown-item">Microsoft</a>
                <a href="#" class="dropdown-item">Telkom</a>
                <a href="#" class="dropdown-item">Indofood</a>
              </div>
            </div>
            <div class="dropdown">
              <button class="btn dropdown-toggle" type="button" id="pilih2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" width="100%">Pilih Program</button>
              <div class="dropdown-menu" aria-labelledby="pilih2" width="100%">
                <a href="#" class="dropdown-item">Gempa</a>
                <a href="#" class="dropdown-item">Tsunami</a>
                <a href="#" class="dropdown-item">Banjir</a>
                <a href="#" class="dropdown-item">Longsor</a>
              </div>
            </div>
          </div>
          <div class="col-sm-9">
          <div class="container">
            <nav class="navbar navbar-light bg-light">
              <span class="navbar-brand mb-0 h1">Microsoft</span>
            </nav>
            <div class="row">
              <div class="col-sm-3">
                <img src="css/img/18.jpg" width="100%">
              </div>
              <div class="col-sm-9">
                <h6>Microsoft Indonesia</h6>
                <h3>Program Siaga Simulasi Bencana Vulkanik</h3>
                <p>Aktifitas vulkanik di Indonesia semakin hari semakin aktif. Oleh karena itu, Microsoft Indonesia bekerjasama dengan R&D Microsoft Corporation merancang program siaga simulasi untuk persiapan terhadap bencana vulkanik.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-9 offset-sm-1">
                <p>DETAIL PROGRAM</p>
              </div>
            </div> 
          </div>
            

            <nav class="navbar navbar-light bg-light">
              <span class="navbar-brand mb-0 h1">Pertamina</span>
            </nav>
            <div class="row">
              <div class="col-sm-3">
                <img src="css/img/20.jpg" width="100%">
              </div>
              <div class="col-sm-9">
                <h6>Pertamina</h6>
                <h3>Pertolongan Petama kepada Korban Longsor</h3>
                <p>Pertamina beserta jajaran tim SAR Indonesia bekerjasama untuk membuat program tanggap darurat terhadap korban longsor..</p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-9 offset-sm-1">
                <p>DETAIL PROGRAM</p>
              </div>
            </div>

            <nav class="navbar navbar-light bg-light">
              <span class="navbar-brand mb-0 h1">Indofood</span>
            </nav>
            <div class="row">
              <div class="col-sm-3">
                <img src="css/img/19.jpg" width="100%">
              </div>
              <div class="col-sm-9">
                <h6>Indofood Industries</h6>
                <h3>Edukasi dan Siaga Awan Panas Vulkanik</h3>
                <p>Edukasi dan Siaga Awan Panas Vulkanik</h3>
                <p>Dengan semakin banyaknya serangan awan panas, Indofood memutuskan untuk membuat program penanggulangan bencana awan panas dengan edukasi..</p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-9 offset-sm-1">
                <p>DETAIL PROGRAM</p>
              </div>
            </div>

          </div>

        </div>
      </div>
    </section>
    <!-- akhir isi -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>