<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link rel="stylesheet" type="text/css" href="css/gaya.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <title>BERANDA</title>
  </head>
  <body>
    <!-- navbar -->
    <nav class="navbar sticky-top navbar-expand-sm navbar-dark bg-light img-fluid">
        <a class="navbar-brand">SIMANPROS</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="#" class="nav-link ">Beranda<span class="sr-only"></span></a>
              </li>
              <li class="nav-item">
                <a href="li_pro.html" class="nav-link">Program</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">Kontak</a>
              </li>
              </li>
              <li class="nav-item">
                <a href="login.html" class="nav-link">Login</a>
              </li>
            </ul>
          </div>
        </nav>
    <!-- akhir navbar -->

    <!-- carousel -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="carousel-caption d-none d-md-block">
        <h1>Melindungi Masyarakat dan Memberdayakan Tanggung Jawab Sosial</h1>
        <p>Memberi kesempaan bagi korporasi untuk melaksanakan CSR dengan efekktif dan efisien</p>
      </div>
      <img class="d-block w-100" src="css/img/13.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <div class="carousel-caption d-none d-md-block">
        <h1>Lihat Program</h1>
        <p>Memberi kesempaan bagi korporasi untuk melaksanakan CSR dengan efekktif dan efisien</p>
      </div>
      <img class="d-block w-100" src="css/img/14.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <div class="carousel-caption d-none d-md-block">
        <h1>Ajukan Proposal</h1>
        <p>Memberi kesempaan bagi korporasi untuk melaksanakan CSR dengan efekktif dan efisien</p>
      </div>
      <img class="d-block w-100" src="css/img/15.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
      <div class="carousel-caption d-none d-md-block">
        <h1>Pantau Pengajuan</h1>
        <p>Memberi kesempaan bagi korporasi untuk melaksanakan CSR dengan efekktif dan efisien</p>
      </div>
      <img class="d-block w-100" src="css/img/16.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    <!-- akhir carousel -->

    <!-- keterangan -->
    <section class="keterangan" id="keterangan">
      <!-- jumbotron -->    
    <!-- akhir jumbotron -->
      <div class="row">
          <div class="col-sm-2">
            <p>Bekerja sama dengan :</p>
          </div>
          <div class="col-sm-2">
            <p>UII</p>
          </div>
          <div class="col-sm-2">
            <p>Microsoft</p>
          </div>
          <div class="col-sm-2">
            <p>Telkom Indonesia</p>
          </div>
          <div class="col-sm-2">
            <p>Pertamina</p>
          </div>
          <div class="col-sm-2">
            <p>Indofood</p>
          </div>
        </div>
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4>Mewujudkan Tanggung Jawab Sosial Korporasi</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 text-center">
          <h1>42</h1><p> Unit Korporat Tergabung dalam SIMANPROS</p>
        </div>
        <div class="col-sm-4 text-center">
          <h1>278</h1><p> Program-Program CSR Terdaftar dalam Database SIMAPNPROS</p>
        </div>
        <div class="col-sm-4 text-center">
          <h1>16</h1><p> X Penerjunan bersama Korporat Membantu Korban Bencana</p>
        </div>
      </div>
    </section>
    <!-- akhir keterangan -->

    <!-- LA -->
    <section class="la">
      <div class="container-fluid" id="la">
        <div class="row">
          <div class="col-sm-6">
            <div class="col-sm-11 offset-sm-1">
              <img src="css/img/3.jpg" width="100%">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-sm-11 offset-sm-1">
              <h1>Lihat Program</h1>
              <p>Lihat beragam program yang sudah diajukan oleh berbagai perusahaan. </p>
              <input type="submit" class="btn btn-primary" value="Lihat Sekarang" id="lihat">
            </div>
          </div>
        </div>
      </div>
      <div class="çontainer-fluid" id="la">
        <div class="row">
        <div class="col-sm-6">
          <div class="col-sm-11 offset-sm-1">
            <h1>Ajukan Proposal</h1>
            <p>Anda memiliki proposal suatu program yang bagus? Ajukan sekarang disini.</p>
            <input type="submit" class="btn btn-primary" value="Ajukan Sekarang" id="ajukan">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="col-sm-11 offset-sm-1">
            <img src="css/img/4.jpg" width="100%">
          </div>
        </div>
      </div>
      </div>
      <div class="container-fluid" id="la">
        <div class="row" id="li">
        <div class="col-sm-6">
          <div class="col-sm-11 offset-sm-1">
            <img src="css/img/5.jpg" width="100%">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="col-sm-11 offset-sm-1">
            <h1>Pantau Pengajuan</h1>
            <p>Pantau semua program yang sudah diajukan dan sedang berjalan.</p>
            <input type="submit" class="btn btn-primary" value="Pantau Sekarang" id="pantau">
          </div>
        </div>
      </div>
      </div>
    </section>
    <!-- akhir LA -->

    <!-- kontribusi -->
    <section class="kontribusi" id="kontribusi">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2>Siap untuk berkontribusi</h1>
          <h4>Bergabung sekarang dan mari lindungi kesejahteraan hidup masyarakat</h3>
          <input type="submit" class="btn btn-primary" value="Kontribusi Sekarang">
        </div>
      </div>
    </section>
    <!-- akhir kontribusi -->

    <!-- meet our team -->
    <section id="team" class="pb-5">
      <div class="container">
        <h5 class="section-title h1">OUR TEAM</h5>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
              <div class="frontside">
                <div class="card">
                  <div class="card-body text-center">
                    <p><img class=" img-fluid" src="css/img/rogeh.jpg" alt="card image"></p>
                    <h4 class="card-title">Geraldy Yusuf Pralampita</h4>
                    <p class="card-text">Back-end Master, khususnya Laravel di dotCom</p>
                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
              <div class="backside">
                <div class="card">
                  <div class="card-body text-center mt-4">
                    <h4 class="card-title">Geraldy Yusuf Pralampita</h4>
                    <p class="card-text">Anggota dotCom yang betugas mengurus Back-end menggunakan Laravel dan merapikan sedikit bagian front-end yang dirasa kurang bagus.</p>
                    <ul class="list-inline">
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-instagram"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-google"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
              <div class="frontside">
                <div class="card">
                  <div class="card-body text-center">
                    <p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_02.png" alt="card image"></p>
                    <h4 class="card-title">M. Asad Arifin H.</h4>
                    <p class="card-text">Front-end Master HTML, Bootstrap, CSS, JS di dotCom</p>
                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
              <div class="backside">
                <div class="card">
                  <div class="card-body text-center mt-4">
                    <h4 class="card-title">M. Asad Arifin H.</h4>
                    <p class="card-text">Anggota dotCom yang mengurusi bagian Front-end secara keseluruhan menggunakan HTML, CSS dan Bootstrap.</p>
                    <ul class="list-inline">
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-instagram"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-google"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ./Team member -->
        <!-- Team member -->
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
            <div class="mainflip">
              <div class="frontside">
                <div class="card">
                  <div class="card-body text-center">
                    <p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_03.png" alt="card image"></p>
                    <h4 class="card-title">Rachmad Arif Naufal</h4>
                    <p class="card-text">Ketua dotCom, Master of Networking</p>
                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
              <div class="backside">
                <div class="card">
                  <div class="card-body text-center mt-4">
                    <h4 class="card-title">Rachmad Arif Naufal</h4>
                    <p class="card-text">Ketua dari dotCom. Mengurusi bagian jaringan khusunya server dan hosting.</p>
                    <ul class="list-inline">
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-facebook"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-twitter"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-instagram"></i>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#">
                          <i class="fa fa-google"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- akhir -->

    <!-- footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-2">
            <h4>SIMANPROS</h4>
            <p>dotCom Corp</p>
          </div>
          <div class="col-sm-2">
            <p>Kontak</p>
            <p>Tentang Kami</p>
          </div>
          <div class="col-sm-2">
            <p>
              <a class="social-icon" href="">
                <i class="fa fa-facebook"></i>
              </a> Facebook
            </p>
            <p>
              <a class="social-icon" href="">
                <i class="fa fa-twitter"></i>
              </a>
               Twitter
             </p>
            <p>
              <a href="" class="social-icon">
                <i class="fa fa-instagram"></i>
              </a>
               Instagram
             </p>
          </div>
          <div class="col-sm-3">
            <p>Subscribe ke harian milis kami</p>
            <form class="form-inline">
              <input type="text" class="form-control" value="Alamat Email Anda">
              <input type="submit" class="btn btn-primary" value="OK">
            </form>
          </div>
          <div class="col-sm-3">
            <p>Jl. Temulawak No. 35C, Sleman, Yogyakarta 55281</p>
            <p>simanpros@gmail.com</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  </body>
</html>