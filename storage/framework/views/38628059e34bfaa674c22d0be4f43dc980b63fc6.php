<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/img/gaya.css">


    <title>Pengajuan Proposal</title>
  </head>
  <body>
    
    <!-- navbar -->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-light">
          <a class="navbar-brand">SIMANPROS</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="li_pro" class="nav-link">Program</a>
              </li>
              <li class="nav-item active">
                <a href="proposal" class="nav-link active">Pengajuan</a>
              </li>
              <li class="nav-item">
                <a href="login" class="nav-link">Logout</a>
              </li>
            </ul>
          </div>
        </nav>
    <!-- akhir navbar -->

    <!-- lihat program -->
    <section class="lipro" id="lipro">
      <div class="container">
        <div class="row">
          <div class="col-sm-5 offset-sm-1">
            <h1>Ajukan Proposal Program Anda</h1>
            <p>Jika anda memiliki program siaga bencana baru, ajukan program tersebut disini</p>
          </div>
        </div>
      </div>
    </section>
    <!-- akhir lihat program -->

    <!-- alur pengajuan -->
    <section class="alur" id="alur">
      <div class="row">
        <div class="col-sm-12">
          <h3 class="text-center">Alur Pengajuan Proposal</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 text-center">
          <img src="css/img/10.png">
          <h5 class="text-center">Buat Akun Anda!</h5>
          <p class="text-center">Buatlah akun anda agar bisa mengajukan proposal program baru</p>
        </div>
        <div class="col-sm-4 text-center">
          <img src="css/img/11.png">
          <h5 class="text-center">isi Form Pengajuan</h5>
          <p class="text-center">isi form prngajuan sesuai dengan formatnya</p>
        </div>
        <div class="col-sm-4 text-center">
          <img src="css/img/12.png">
          <h5 class="text-center">Unggah Proposal Anda</h5>
          <p class="text-center">Unggah proposal anda untuk segera diproses</p>
        </div>
      </div>
    </section>
    <!-- akhir alur pengajuan -->

    <!-- form pengajuan -->
    <section class="aju" id="aju">
      <div class="container">
        <div class="row">
          <h1>Halo !</h1>
        </div>
        <div class="row">
          <h4>Silahkan isi form untuk mengajukan program baru</h4>
        </div>
       

        

          <form action="<?php echo e(route('proposal.store')); ?>" method="post">
            <?php echo e(csrf_field()); ?>


        <div class="row">
          <div class="col-sm-4" name="kategori">
            <h5>Kategori</h5>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kategori" id="exampleRadios1" value="option1" checked>
              <label class="form-check-label" for="exampleRadios1">Gempa Bumi</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="option2">
              <label class="form-check-label" for="exampleRadios2">Tanah Longsor</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="option2">
              <label class="form-check-label" for="exampleRadios2">Gunung Meletus</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="option2">
              <label class="form-check-label" for="exampleRadios2">Banjir</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="option2">
              <label class="form-check-label" for="exampleRadios2">Angin Puting Beliung</label>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group">
              <label>Judul Program</label>
              <input type="text" class="form-control" placeholder="Judul Program" name="judul">
            </div>
            <div class="form-group">
              <label>Tujuan Program</label>
              <input type="text" class="form-control" placeholder="Tujuan Program" name="tujuan">
            </div>
            <div class="form-group">
              <label>Sasaran Program</label>
              <input type="text" class="form-control" placeholder="Sasaran Program" name="sasaran_program">
            </div>
            <div class="form-group">
              <label>Lokasi Program</label>
              <input type="text" class="form-control" placeholder="Lokasi Program" name="lokasi">
            </div>
            <div class="form-group">
              <label>Biaya Program</label>
              <input type="text" class="form-control" placeholder="Biaya Program" name="biaya">
            </div>
            <div class="text-center">
              <h5>Lampirkan File Anda di Bawah</h5>
              <p>Hanya format berikut yang diperboehkan : .doc, .pptx, .pdf, .jpg, .mp4</p>
            </div>
                        <form>
              <div class="form-group"><label for="exampleFormControlFile1">Pilih File</label>
                <input type="file" class="form-control-file" id="uploadfile" name="file">
              </div>
            </form>
             <div class="col-sm-12 text-center">
          <button type="submit" class="btn btn-primary">Ajukan Program</button>
        </div>

          </form>
        </div>
      </div>
      <div class="row">
       
      </div>
    </div>
    </section>
    <!-- akhir form pengajuan -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>