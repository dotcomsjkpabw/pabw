<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/img/gaya.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <title>LOGIN</title>
  </head>
  <body id="badan">
    <!-- navbar -->
    <!-- navbar -->
    <nav class="navbar sticky-top navbar-expand-sm navbar-dark bg-light img-fluid">
        <a class="navbar-brand">SIMANPROS</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link ">Beranda<span class="sr-only"></span></a>
              </li>
              <li class="nav-item">
                <a href="li_pro" class="nav-link">Program</a>
              </li>
              <li class="nav-item">
                <a href="index.php/#kontak" class="nav-link">Kontak</a>
              </li>
              </li>
              <li class="nav-item">
                <a href="login" class="nav-link">Login</a>
              </li>
            </ul>
          </div>
        </nav>
    <!-- akhir navbar -->

    <!-- login -->
    <div class="card" style="width: 20rem;">
      <div class="card-body">
        <h4 class="card-tittle text-center">Login</h4>

        <form action="<?php echo e(route('korporat.store')); ?>" method="post">
          <?php echo e(csrf_field()); ?>

          <div class="form-group text-center">
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <span>
                    <i class="fa fa-envelope"></i>
                  </span>
                </div>
              </div>
              <input class="form-control" type="text" placeholder="Email address">
            </div>
          </div>
          <div class="form-group text-center">
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <span>
                    <i class="fa fa-lock"></i>
                  </span>
                </div>
              </div>
              <input class="form-control" type="password" placeholder="Password">
            </div>
          </div>
        </div>
          <div class="form-group text-center">
            <div class="btn-group" role="group">
              <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Masuk Sebagai</button>
              <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="lipro_kor">Korporat</a>
                <a class="dropdown-item" href="bpdb">BPDB</a>
              </div>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Daftar</button>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Buat Akun Agar Bisa Mengajukan Program</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <form >
                      <?php echo e(csrf_field()); ?>

                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama Perusahaan</label>
                        <input type="text" class="form-control" id="recipient-name" name="nama_perusahaan" placeholder="Masukkan Nama Perusahaan">
                      </div>
                      <div class="form-group">
                        <label for="message-text" class="col-form-label">Bentuk Perusahaan</label>
                        <select class="form-control" name="bentuk_perusahaan">
                          <option>(Pilih Bentuk Perusahaan</option>
                          <option>PT (Perseroan Terbatas)</option>
                          <option>CV</option>
                          <option>Koperasi</option>
                          <option>BUMN</option>
                          <option>Pemerintah</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Email</label>
                        <input type="email" class="form-control" id="email_korporat" name="email" placeholder="Masukkan Alamat Email">
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Password</label>
                        <input type="password" class="form-control" id="pass_korporat" name="password" placeholder="Masukkan Password">
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">No. Telepon</label>
                        <input type="text" class="form-control" id="telp_korporat" name="no_telepon" placeholder="Masukkan Nomor Telepon">
                      </div>
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Website</label>
                        <input type="text" class="form-control" id="web_korporat" name="website" placeholder="Masukkan Alamat Website">
                      </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Daftar!</button>
                  </div>
                    </form>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- halaman login -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>