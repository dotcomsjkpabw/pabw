<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('korporat','korporatController');
Route::resource('program', 'ProgramController');
Route::resource('akun', 'AkunController');
Route::resource('proposal','proposalController');
Route::resource('kategorisasi','kategorisasiController');
Route::resource('bpdb','bpdbController');
Route::resource('data_master','data_masterController');

Route::get('/kategorisasi', 'kategorisasiController@index')->name('kategorisasi');



Route::get('/', function () {
    return view('index');
});

Route::get('/li_pro', function () {
    return view('li_pro');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/lipro_kor', function () {
    return view('lipro_kor');
});

Route::get('/bpdb', function () {
    return view('bpdb');
});

Route::get('/akun_pengguna', function () {
    return view('akun_pengguna');
});

Route::get('/data_master', function () {
    return view('data_masterView');
});

Route::get('/kategorisasi', function () {
    return view('kategorisasi');
});